/*

 */
package cest.edu.aval2;

/**
 *
 * @author johna
 */
public class UF {
    private String sigla;
    private String descricao;

    public UF(String sigla, String descricao) {
        this.sigla = sigla;
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "UF{" + "sigla=" + sigla + ", descricao=" + descricao + '}';
    }
    

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
}

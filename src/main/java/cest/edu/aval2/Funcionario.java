
package cest.edu.aval2;

import java.math.BigDecimal;

/**
 *
 * @author johna
 */
public class Funcionario extends PessoaFisica {
    public String cargo;
    public BigDecimal salario;

    public Funcionario(String cargo, BigDecimal salario, String nome, String cpf) {
        super(nome, cpf);
        this.cargo = cargo;
        this.salario = salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }
    

    
    
            
    
}

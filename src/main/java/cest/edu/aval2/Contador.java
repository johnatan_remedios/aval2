/*

 */
package cest.edu.aval2;

/**
 *
 * @author johna
 */
public class Contador {
    private int numero;
    private Contador(){};
    
    private static Contador c = new Contador();
    
    public static Contador getInstance(){
        return c;
    }
    public int proximoNumero(){
        int i = 0;
        return ++i;
                
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
}

/*

 */
package cest.edu.aval2;

/**
 *
 * @author johna
 */
public class Cidade extends UF {
    private String nome;

    public Cidade(String nome, String sigla, String descricao) {
        super(sigla, descricao);
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String getSigla() {
        return super.getSigla(); 
    }

  
    
}

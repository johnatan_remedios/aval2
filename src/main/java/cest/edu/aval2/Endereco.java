/*

 */
package cest.edu.aval2;

/**
 *
 * @
 */
public class Endereco extends Cidade {
    private String logradouro;

    public Endereco(String logradouro, String nome, String sigla, String descricao) {
        super(nome, sigla, descricao);
        this.logradouro = logradouro;
    }

   

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }
    
}

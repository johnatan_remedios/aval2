
package cest.edu.aval2;

/**
 *
 * @author johna
 */
public class PessoaJuridica  {
    private String cnpj;
    private String nome;

    public PessoaJuridica(String cnpj, String nome) {
        this.cnpj = cnpj;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "{CNPJ:" +cnpj +"Nome: "+nome +"}";
    }
    

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    

    
    
    
}

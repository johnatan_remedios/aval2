/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cest.edu.aval2;

import java.util.Date;

/**
 *
 * @author johna
 */
public class Atendimento {
    private Date data;
    private int numero;

    public Atendimento(Date data, int numero) {
        this.data = data;
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Atendimento{" + "data=" + data + ", numero=" + numero + '}';
    }



    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
    
}
